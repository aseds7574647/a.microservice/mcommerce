// src/components/Accueil.js
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { getProducts } from '../services/api';

const Accueil = () => {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const data = await getProducts();
                setProducts(data);
            } catch (error) {
                console.error(error);
            }
        };

        fetchData();
    }, []);

    return (
        <div className={"body"}>
            <h2>Application Mcommerce</h2>
            <ul>
                {products.map(product => (
                    <li key={product._id}>
                        <Link to={`/fiche-produit/${product._id}`} className={"image"}>
                            <img src={ product.imageUrl } alt={"Produit"} width={'100%'}/>
                            {product.title}
                        </Link>
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default Accueil;
