// frontend/services/api.js

const BASE_URL = 'http://localhost:9000/api';

const getAuthHeaders = () => {
    const token = localStorage.getItem('token');
    if (token) {
        return {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        };
    }
    return {
        'Content-Type': 'application/json'
    };
};

const fetchWithAuth = async (url, options) => {
    const headers = getAuthHeaders();
    return await fetch(url, {...options, headers});
};

export const getProducts = () => {
    return fetch(`${BASE_URL}/getAllThings`)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        });
};

export const getProduct = (productId) => {
    return fetch(`${BASE_URL}/getOneThing/${productId}`)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        });
};

export const createOrder = (title, callback) => {
    fetchWithAuth(`${BASE_URL}/addOrder`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            title: `${title}`
        }),
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Erreur lors de la création de la commande.');
            }
            return response.json();
        })
        .then(data => {
            callback(null, data);
        })
        .catch(error => {
            callback(error, null);
        });
};


export const confirmPayment = async (orderId) => {
    try {
        const response = await fetchWithAuth(`${BASE_URL}/payOrder`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idCommande: orderId,  // Assurez-vous que le microservice paiement attend un champ idCommande
            }),
        });

        if (!response.ok) {
            throw new Error('Erreur lors de la confirmation du paiement.');
        }

        return await response.json();
    } catch (error) {
        throw new Error('Erreur lors de la confirmation du paiement : ' + error.message);
    }
};

export const auth = {
    login: async (email, password) => {
        try {
            const response = await fetch(`${BASE_URL}/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    email: email,
                    password: password,
                }),
            });

            if (!response.ok) {
                throw new Error('Erreur lors de l\'inscription.');
            }

            // Convertissez la réponse en JSON
            const data = await response.json();

            return data;
        } catch (error) {
            throw error;
        }
    },

    register: async (email, password) => {
        try {
            const response = await fetch(`${BASE_URL}/register`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    email: email,
                    password: password,
                }),
            });
            if (!response.ok) {
                // Si la réponse n'est pas OK, extrayez le message d'erreur et renvoyez-le
                const errorResponse = await response.json(); // Assurez-vous que le serveur renvoie une réponse JSON
                const errorMessage = errorResponse.error;
                throw new Error(errorMessage);
            }

            // Convertissez la réponse en JSON
            return await response.json();
        } catch (error) {
            throw error;
        }
    },

};