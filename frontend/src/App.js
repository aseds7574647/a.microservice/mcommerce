// src/App.js
import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Accueil from './components/Accueil';
import FicheProduit from './components/FicheProduit';
import Paiement from './components/Paiement';
import Authentification from "./components/Authentification";
import Inscription from "./components/Inscription";

const App = () => {
    return (
        <Router>
            <Routes>
                <Route path="/" element={<Accueil />} />
                <Route path="/fiche-produit/:id" element={<FicheProduit />} />
                <Route path="/paiement/:id" element={<Paiement />} />
                <Route path="/auth" element={<Authentification />} />
                <Route path="/inscription" element={<Inscription />} />
            </Routes>
        </Router>
    );
};

export default App;
